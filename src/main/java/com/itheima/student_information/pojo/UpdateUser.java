package com.itheima.student_information.pojo;

public class UpdateUser {
    private String username;
    private Integer age;
    private Integer gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "updateUserByUsername{" +
                "username='" + username + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}
