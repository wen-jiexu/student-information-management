package com.itheima.student_information.controller;

import com.itheima.student_information.pojo.NewStudent;
import com.itheima.student_information.pojo.Student;
import com.itheima.student_information.pojo.UpdateUser;
import com.itheima.student_information.service.StudentService;
import com.itheima.student_information.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService stuService;

/**注册*/
    @RequestMapping("/user/register")
    public Result register(@RequestBody Student student) {

        Result result = stuService.register(student);
        return result;
    }
/**登录*/
    @RequestMapping("/user/login")
    public Result login(@RequestBody Student student) {

        Result result = stuService.login(student);
        return result;
    }
/**根据用户名查询学生信息*/
    @RequestMapping("/user/{username}")
    public Result inquireName(@PathVariable String username) {
        Result result = stuService.inquireName(username);
        return result;
    }
/**根据用户名删除学生信息*/
    @RequestMapping("/user/del")
    public Result del(String username) {
        Result result = stuService.del(username);
        return result;
    }

/**根据用户名修改学生密码*/
    @RequestMapping("/user/updatePassword")
    public Result updatePassword(@RequestBody NewStudent nuStudent) {
        Result result = stuService.updatePassword(nuStudent);
        return result;
    }
/**查询所有注册学生的列表*/
    @RequestMapping("/user/list")
    public Result list() {
        Result result = stuService.list();
        return result;
    }
/**根据用户名修改学生信息*/
    @RequestMapping("/user/updateUserByUsername")
    public Result updateUserByUsername(@RequestBody UpdateUser updateUser) {
        Result result = stuService.updateUser(updateUser);
        return result;
    }
/**根据学生姓名批量删除学生*/
    @RequestMapping("/user/delBatch")
    public Result delBatch(@RequestParam List<String> hobby) {
        Result result = stuService.delBatch(hobby);
        return result;
    }

}
