package com.itheima.student_information.dao.Impl;

import com.itheima.student_information.dao.StudentDao;
import com.itheima.student_information.pojo.Student;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDaoImpl implements StudentDao {
    //存放学生信息的集合
    private List<Student> list = new ArrayList<>();

    @Override
    public List<Student> register() {
        return list;
    }

    //存储学生信息
    @Override
    public void resave(Student student1) {
        list.add(student1);
    }

    @Override
    public Student inquireName(String username) {
        //根据用户名获取对象所在索引
        int index = indexName(username);
        if (index==-1){
            return null;
        }
        return list.get(index);

    }

    @Override
    public boolean del(String username) {
        int index = indexName(username);
        if (index==-1){
            return false;
        }
        list.remove(index);
        return true;

    }

    @Override
    public void updatePassword(int index, String nPassword) {
        Student student = list.get(index);
        student.setPassword(nPassword);
        list.remove(index);
        list.add(student);
    }

    @Override
    public void updateUser(String name, Integer upAge, Integer upGender) {
        int i = indexName(name);
        list.get(i).setGender(upGender);
        list.get(i).setAge(upAge);
    }

    @Override
    public void remove(String s) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUsername().equals(s)){
                list.remove(i);
                break;
            }
        }
    }

    @Override
    public List<Student> getList() {
        return list;
    }

    int indexName(String username) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUsername().equals(username)){
                index = i;
                break;
            }
        }
        return index;
    }

}
