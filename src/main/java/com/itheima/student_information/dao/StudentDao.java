package com.itheima.student_information.dao;

import com.itheima.student_information.pojo.Student;

import java.util.List;

public interface StudentDao {
    List<Student> register();

    void resave(Student student1);

    Student inquireName(String username);

    boolean del(String username);

    void updatePassword(int index, String nPassword);

    void updateUser(String name, Integer upAge, Integer upGender);

    void remove(String s);

    List<Student> getList();

}
