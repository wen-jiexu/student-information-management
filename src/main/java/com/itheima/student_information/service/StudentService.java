package com.itheima.student_information.service;

import com.itheima.student_information.pojo.NewStudent;
import com.itheima.student_information.pojo.Student;
import com.itheima.student_information.pojo.UpdateUser;
import com.itheima.student_information.utils.Result;

import java.util.List;

public interface StudentService {
    Result register(Student student);

    Result login(Student student);


    Result inquireName(String username);

    Result del(String username);

    Result updatePassword(NewStudent nuStudent);

    Result list();

    Result updateUser(UpdateUser updateUser);

    Result delBatch(List<String> hobby);
}
