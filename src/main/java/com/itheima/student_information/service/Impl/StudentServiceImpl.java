package com.itheima.student_information.service.Impl;

import com.itheima.student_information.dao.StudentDao;
import com.itheima.student_information.pojo.NewStudent;
import com.itheima.student_information.pojo.Student;
import com.itheima.student_information.pojo.UpdateUser;
import com.itheima.student_information.service.StudentService;
import com.itheima.student_information.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;


    @Override
    public Result register(Student student) {
        // 注册一个学生信息，要求填写用户名，年龄，性别，密码，
        List<Student> list = studentDao.register();
        Student student1 = new Student();
        /**判断注册账号是否满足需求的逻辑*/

        // 1. 用户名必须是英文字符，不能出现中文字符，不能出现空格等特殊字符
        char[] chars = student.getUsername().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] > 'z' || chars[i] < 'A' || (chars[i] > 'Z' && chars[i] < 'a')) {
                return Result.error("您的用户名中含有非法字符");
            }
        }


        if (list != null) {
            //2. 用户名不能重复，已经存在的用户返回用户已存在
            for (Student student2 : list) {
                if (student2.getUsername().equals(student.getUsername())) {
                    return Result.error("用户名重复");
                }
            }
        }

        student1.setUsername(student.getUsername());

        //密码如果未填写，则默认是123456
        if (student.getPassword() == null) {
            student1.setPassword("123456");
        }

        // 3. 密码必须在6~16位之前，否则返回密码不符合要求
        if (student.getPassword().length() < 6 || student.getPassword().length() > 16) {
            return Result.error("密码不符合要求");

        } else {
            student1.setPassword(student.getPassword());
        }
        //4. 性别是选填项，可选值为1、2和空
        //(student.getGender()!=1) || (student.getGender()!=2)||(student.getGender() != null)
        if (student.getGender() == 1 || student.getGender() == 2 || student.getGender() == null) {
            student1.setGender(student.getGender());
        } else {
            return Result.error("性别不符合要求");
        }

        //5. 年龄是数字
        student1.setAge(student.getAge());

        //list.add(student1);

        studentDao.resave(student1);
        return Result.success(student1);
    }

    @Override
    public Result login(Student student) {
//        1. 用户登录成功必须是用户名存在且密码对应正确
//        2. 用户不存在返回用户不存在提示
//        3. 密码不正确返回密码不正确提示
        List<Student> list = studentDao.register();
        if (list.size() == 0) {
            return Result.error("系统中没有账号数据");
        }
        boolean flag = false;
        Student student1 = new Student();
        for (Student stu : list) {
            if (stu.getUsername().equals(student.getUsername())) {
                student1.setUsername(stu.getUsername());
                student1.setAge(stu.getAge());
                student1.setGender(stu.getGender());
                student1.setPassword("******");
                flag = true;
            }
        }

        if (flag == false) {
            return Result.error("用户不存在");
        }

        for (Student stu : list) {
            if (stu.getPassword().equals(student.getPassword())) {
                return Result.success(student1);
            }
        }

        return Result.error("密码不正确");
    }

    @Override
    public Result inquireName(String username) {
        //Student student = studentDao.inquireName(username);
        List<Student> stuList = studentDao.getList();
        for (Student student : stuList) {
            if(username.equals(student.getUsername())){
                Student s = new Student();
                s.setUsername(student.getUsername());
                s.setPassword("******");
                s.setAge(student.getAge());
                s.setGender(student.getGender());
                return Result.success(s);
            }
        }
        return Result.error("用户不存在");
    }

    @Override
    public Result del(String username) {
        boolean flag = studentDao.del(username);
        if (flag) {
            return Result.success();
        }
        return Result.error("用户名不存在");
    }

    @Override
    public Result updatePassword(NewStudent nuStudent) {

        List<Student> list = studentDao.register();

        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUsername().equals(nuStudent.getUsername())) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return Result.error("用户名不存在");
        }
        if (!(list.get(index).getPassword().equals(nuStudent.getPassword()))) {
            return Result.error("旧密码验证不通过");
        }
        if (nuStudent.getNewPassword().length() < 6 || nuStudent.getNewPassword().length() > 16) {
            return Result.error("新密码不符合要求");
        }
        String nPassword = nuStudent.getNewPassword();
        studentDao.updatePassword(index, nPassword);

        return Result.success();
    }

    @Override
    public Result list() {
        List<Student> stuList = studentDao.register();
        if (stuList.size() == 0) {
            return Result.error("系统中没有学生数据");
        }
        List<Student>sList = new ArrayList<>();

        for (Student student : stuList) {
            Student s = new Student();
            s.setUsername(student.getUsername());
            s.setAge(student.getAge());
            s.setGender(student.getGender());
            s.setPassword("******");
            sList.add(s);
        }
        return Result.success(sList);
    }

    @Override
    public Result updateUser(UpdateUser updateUser) {

        List<Student> stuList = studentDao.register();
        String name = updateUser.getUsername();

        if (stuList.size() == 0) {
            return Result.error("系统中没有学生数据");
        }
        boolean flag = false;
        Student stu = new Student();
        for (Student student : stuList) {
            if (student.getUsername().equals(name)) {
                stu.setPassword("******");
                flag = true;
                break;
            }
        }

        Integer upAge = updateUser.getAge();
        Integer upGender = updateUser.getGender();

        if (flag) {
            studentDao.updateUser(name, upAge, upGender);
        } else {
            return Result.error("用户名不存在");
        }
        //Student student = new Student(name,upAge,upGender,passWorld);
        stu.setAge(upAge);
        stu.setGender(upGender);
        stu.setUsername(name);
        return Result.success(stu);
    }

    @Override
    public Result delBatch(List<String> hobby) {
        for (String s : hobby) {
            studentDao.remove(s);
        }
        return Result.success();
    }
}
